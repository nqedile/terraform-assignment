import json
import boto3

def lambda_handler(event, context):

    bucket = "abc-baseball-card-bucket"
    card = "card_info.png"
    
    sns = boto3.client('sns')
    client = boto3.client('rekognition', region_name = 'eu-west-1')
    dynamodb = boto3.client('dynamodb', region_name = 'eu-west-1')

    response = client.detect_text(Image = {"S3Object": {"Bucket": bucket, "Name": card}})
    
    card_text = ""
    for TextDetected in response['TextDetections']:
        card_text += TextDetected['DetectedText'] + " "   

    ############## CREATING DYNAMODB TABLE ################

    dynamodb.put_item(
        TableName = 'card-db',
        Item = {
            'card_info': { 'S': 'NQ.edile' },
            'message': { 'S': card_text }
        })
    
    sns.publish(
        TopicArn = "arn:aws:sns:eu-west-1:296274010522:baseball-card-sns",
        Message = "A new card has been uploaded.",
        Subject = 'New Card Uploaded'
    )

    return {
        'statusCode': 200,
        'body': json.dumps(card_text)
    }  

