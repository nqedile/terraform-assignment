import boto3
import json

dynamodb = boto3.client('dynamodb', region_name = 'eu-west-1')

def rest_api(event, context):

    response = dynamodb.get_item(
        TableName = 'card-db',
        Key = {
            'card_info': {'S': 'NQ.edile'}
        })

    return json.loads('{"isBase64Encoded": false, "statusCode": 200, "body": "'+response+'"}')
    