terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.5.0"
    }
  }
}

############## POLICY ################
resource "aws_iam_role_policy" "lambda_policy" {
  name = "lambda_policy"
  role = aws_iam_role.lambda_role.id

  policy = "${file("iam/lambda-iam-policy.json")}"
}

############## ASUMMING ROLE ################
resource "aws_iam_role" "lambda_role" {
  name = "lambda_role"

  assume_role_policy = "${file("./iam/lambda-iam-role.json")}"
}

############## CREATING S3 BUCKET ################
resource "aws_s3_bucket" "s3_for_terraform_assignment" {
  bucket = "abc-baseball-card-bucket"
  acl    = "private"

  tags = {
    Name        = "Bucket for My card"
    Environment = "Dev"
  }
}

############## PERMISSION FOR LAMBDA TO USE S3 BUCKET ################
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AmazonS3ReadOnlyAccess"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.my_lambda_function.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_for_terraform_assignment.arn
}

############## PERMISSION FOR REKOGNITION ################
resource "aws_lambda_permission" "rekog" {
  statement_id  = "AmazonRekognitionFullAccess"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.my_lambda_function.arn
  principal     = "rekognition.amazonaws.com"
#   source_arn    = aws_sns_topic.default.arn
}

############## ARCHIVE FILE ################
data "archive_file" "lambda" {
  type        = "zip"
  source_file = "${path.module}/lambda-func.py"
  output_path = "${path.module}/lambda-func.py.zip"
}

############## SETTING UP LAMBDA RESOURCE ################
resource "aws_lambda_function" "my_lambda_function" {
  filename      = data.archive_file.lambda.output_path
  function_name = "access_s3_bucket"
  timeout       = 100
  role          = aws_iam_role.lambda_role.arn
  handler       = "lambda-func.lambda_handler"
  runtime       = "python3.8"
  source_code_hash = filebase64sha256(data.archive_file.lambda.output_path)
}


############## SETTING UP S3 NOTIFICATION ################
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_for_terraform_assignment.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.my_lambda_function.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}


