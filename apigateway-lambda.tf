
############## API GATEWAY ################
resource "aws_api_gateway_rest_api" "api" {
  name = "myapi"
}

############## API GATEWAY ################
resource "aws_api_gateway_resource" "resource" {
  path_part   = "resource"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.my_lambda.invoke_arn
}

############## LAMBDA PERMISSION FOR API GATEWAY ################
resource "aws_lambda_permission" "getway_perm" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.my_lambda.function_name
  principal     = "apigateway.amazonaws.com"

#   source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.api.id}/*/GET/resource"
  source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
}

data "archive_file" "gateway" {
  type        = "zip"
  source_file = "${path.module}/api-gateway-func.py"
  output_path = "${path.module}/api-gateway-func.py.zip"
}

############## API GATEWAY LAMBDA FUNCTION ################
resource "aws_lambda_function" "my_lambda" {
  filename      = data.archive_file.gateway.output_path
  function_name = "gateway_func"
  role          = aws_iam_role.lambda_role.arn
  handler       = "api-gateway-func.rest_api"
  runtime       = "python3.8"
  source_code_hash = filebase64sha256(data.archive_file.gateway.output_path)
}